import React from "react";

const Tags = (props) => {

  return (
    <div className='tags'>
      {
        props.tags.map(tag => <h1 className='tag'>#{tag}</h1>)
      }
    </div>
  )
}

// function Tags(tags=[]) {
//   return tags.tag.map((tag, index) => <div key={index} className="tag">{'#' + tag}</div>)
// }
export default Tags;